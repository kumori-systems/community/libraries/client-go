/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	v1beta1 "gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/generated-clusterapi/clientset/versioned/typed/clusterapi/v1beta1"
	rest "k8s.io/client-go/rest"
	testing "k8s.io/client-go/testing"
)

type FakeClusterV1beta1 struct {
	*testing.Fake
}

func (c *FakeClusterV1beta1) Clusters(namespace string) v1beta1.ClusterInterface {
	return &FakeClusters{c, namespace}
}

func (c *FakeClusterV1beta1) ClusterClasses(namespace string) v1beta1.ClusterClassInterface {
	return &FakeClusterClasses{c, namespace}
}

func (c *FakeClusterV1beta1) Machines(namespace string) v1beta1.MachineInterface {
	return &FakeMachines{c, namespace}
}

func (c *FakeClusterV1beta1) MachineDeployments(namespace string) v1beta1.MachineDeploymentInterface {
	return &FakeMachineDeployments{c, namespace}
}

func (c *FakeClusterV1beta1) MachineHealthChecks(namespace string) v1beta1.MachineHealthCheckInterface {
	return &FakeMachineHealthChecks{c, namespace}
}

func (c *FakeClusterV1beta1) MachineSets(namespace string) v1beta1.MachineSetInterface {
	return &FakeMachineSets{c, namespace}
}

// RESTClient returns a RESTClient that is used to communicate
// with API server by this client implementation.
func (c *FakeClusterV1beta1) RESTClient() rest.Interface {
	var ret *rest.RESTClient
	return ret
}
