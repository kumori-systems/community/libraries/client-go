/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by lister-gen. DO NOT EDIT.

package v1alpha6

import (
	v1alpha6 "gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/apis_openstack/openstack/v1alpha6"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// OpenStackClusterLister helps list OpenStackClusters.
// All objects returned here must be treated as read-only.
type OpenStackClusterLister interface {
	// List lists all OpenStackClusters in the indexer.
	// Objects returned here must be treated as read-only.
	List(selector labels.Selector) (ret []*v1alpha6.OpenStackCluster, err error)
	// OpenStackClusters returns an object that can list and get OpenStackClusters.
	OpenStackClusters(namespace string) OpenStackClusterNamespaceLister
	OpenStackClusterListerExpansion
}

// openStackClusterLister implements the OpenStackClusterLister interface.
type openStackClusterLister struct {
	indexer cache.Indexer
}

// NewOpenStackClusterLister returns a new OpenStackClusterLister.
func NewOpenStackClusterLister(indexer cache.Indexer) OpenStackClusterLister {
	return &openStackClusterLister{indexer: indexer}
}

// List lists all OpenStackClusters in the indexer.
func (s *openStackClusterLister) List(selector labels.Selector) (ret []*v1alpha6.OpenStackCluster, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*v1alpha6.OpenStackCluster))
	})
	return ret, err
}

// OpenStackClusters returns an object that can list and get OpenStackClusters.
func (s *openStackClusterLister) OpenStackClusters(namespace string) OpenStackClusterNamespaceLister {
	return openStackClusterNamespaceLister{indexer: s.indexer, namespace: namespace}
}

// OpenStackClusterNamespaceLister helps list and get OpenStackClusters.
// All objects returned here must be treated as read-only.
type OpenStackClusterNamespaceLister interface {
	// List lists all OpenStackClusters in the indexer for a given namespace.
	// Objects returned here must be treated as read-only.
	List(selector labels.Selector) (ret []*v1alpha6.OpenStackCluster, err error)
	// Get retrieves the OpenStackCluster from the indexer for a given namespace and name.
	// Objects returned here must be treated as read-only.
	Get(name string) (*v1alpha6.OpenStackCluster, error)
	OpenStackClusterNamespaceListerExpansion
}

// openStackClusterNamespaceLister implements the OpenStackClusterNamespaceLister
// interface.
type openStackClusterNamespaceLister struct {
	indexer   cache.Indexer
	namespace string
}

// List lists all OpenStackClusters in the indexer for a given namespace.
func (s openStackClusterNamespaceLister) List(selector labels.Selector) (ret []*v1alpha6.OpenStackCluster, err error) {
	err = cache.ListAllByNamespace(s.indexer, s.namespace, selector, func(m interface{}) {
		ret = append(ret, m.(*v1alpha6.OpenStackCluster))
	})
	return ret, err
}

// Get retrieves the OpenStackCluster from the indexer for a given namespace and name.
func (s openStackClusterNamespaceLister) Get(name string) (*v1alpha6.OpenStackCluster, error) {
	obj, exists, err := s.indexer.GetByKey(s.namespace + "/" + name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(v1alpha6.Resource("openstackcluster"), name)
	}
	return obj.(*v1alpha6.OpenStackCluster), nil
}
